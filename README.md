# Annoweb
This plugin will enhance the user-experience on Chrome and Firefox by providing a way for users to annotate web pages.

While there is a plugin to annotate webpages(https://hypothes.is/), it requires an account to take notes/highlight anything on the webpage.
## Planned Features
<ul>
  <li>
    Customizable Categories
  </li>
    <li>
    Customizable Colors for Highlights
  </li>
  <li>
    Styled Comments--WWYISG editor
  </li>
</ul>

## Planned Features v2
<ul>
  <li>
    Sync
      <li>
        FireFox mobile
      </li>
  </li>
</ul>
